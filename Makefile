# vim: syntax=make noexpandtab
CWD	=$(shell pwd)
APACHE_SOURCE="http://apache.xl-mirror.nl/activemq/activemq-apollo/1.7.1/apache-apollo-1.7.1-unix-distro.tar.gz"

build:
	mkdir $(CWD)/build
	wget $(APACHE_SOURCE)
	tar -zxvf apache-apollo-*-unix-distro.tar.gz
	mv $(CWD)/apache-apollo-*/* $(CWD)/build/
	rm -rf $(CWD)/build/docs
	rm -rf $(CWD)/build/examples

clean:
	rm -rf apache-apollo-*
